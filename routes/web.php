<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/login', 'AuthController@formLogin')->middleware(['guest']);
Route::post('/login', 'AuthController@doLogin')->middleware(['guest']);
Route::get('/register', 'Auth\RegisterController@formRegister')->middleware(['guest']);
Route::post('/register', 'Auth\RegisterController@create')->middleware(['guest']);
Route::get('/frameworks/{id}', 'FrameworksController@ver')->where('id', '\d+');

Route::middleware(['auth'])->group(function() {
    Route::get('/logout', 'AuthController@doLogout');
    Route::post('/comentarios/new/', 'ComentariosController@crear');
    Route::put('/user', 'UserController@edit');
    Route::put('/user/pass', 'UserController@editPass');
});

Route::middleware(['admin'])->group(function(){
    Route::get('/frameworks/new', 'FrameworksController@formNuevo');
    Route::get('/admin', 'AdminController@index');
    Route::get('/admin/comentarios', 'AdminController@comentarios');
    Route::post('/frameworks/new', 'FrameworksController@crear');
    Route::get('/frameworks/edit/{id}', 'FrameworksController@formEdit')->where('id', '\d+');
    Route::put('/frameworks/{id}', 'FrameworksController@edit')->where('id', '\d+');
    Route::delete('/frameworks/delete/{id}', 'FrameworksController@eliminar');
    Route::delete('/comentarios/delete/{id}', 'ComentariosController@eliminar');
});