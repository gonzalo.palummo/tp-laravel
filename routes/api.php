<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::get('frameworks', 'API\\FrameworksController@index');
Route::get('frameworks/{id}', 'API\\FrameworksController@ver');

Route::get('comentarios', 'API\\ComentariosController@index');
Route::get('comentarios/{id}', 'API\\ComentariosController@ver');

Route::get('users', 'API\\UsersController@index');
Route::get('users/{id}', 'API\\UsersController@ver');

Route::middleware(['auth:api', 'token-admin'])->group(function() {
    Route::put('frameworks/{id}', 'API\\FrameworksController@edit');
    Route::post('frameworks', 'API\\FrameworksController@crear');
    Route::delete('frameworks/{id}', 'API\\FrameworksController@eliminar');

    Route::put('comentarios/{id}', 'API\\ComentariosController@edit');
    Route::delete('comentarios/{id}', 'API\\ComentariosController@eliminar');
});

Route::middleware(['auth:api'])->group(function() {
    Route::post('comentarios', 'API\\ComentariosController@crear')->middleware(['check-user-token']);
    Route::put('users', 'API\\UsersController@edit');
    Route::put('users/pass', 'API\\UsersController@editPass');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('signup', 'API\\AuthController@signup');
    Route::post('login', 'API\\AuthController@login');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'API\\AuthController@logout');
        Route::get('user', 'API\\AuthController@user');
    });
});
