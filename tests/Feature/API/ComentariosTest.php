<?php

namespace Tests\Feature\API;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ComentariosTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function setUp() : void
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function testIndexTraeTodosLosComentarios()
    {
        $response = $this->json('get','/api/comentarios');

        $response->assertStatus(200);
        $response->assertJsonCount(2, 'data');
    }

    public function testVerTraeElComentarioCorrecto()
    {
        $id = 1;
        $response = $this->json('get', 'api/comentarios/' . $id);

        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => ['id' => $id]]);
    }

    public function testVerFallaAlBuscarUnComentarioInexistente()
    {
        $id = 1000;
        $response = $this->json('get', 'api/comentarios/' . $id);

        $response->assertStatus(404);
    }

    public function testEditarLoHaceCorrectamente()
    {
        $id = 2;
        $data = [
            'contenido' => 'Me gusta este'
        ];

        $response = $this->json('put','api/comentarios/'.$id, $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => $data]);
    }

    public function testEditarMuestraErroresDeValidacionConDataErronea()
    {
        $id = 2;
        $data = [
            'contenid' => ''
        ];

        $response = $this->json('put','api/comentarios/'.$id, $data);
        $response->assertStatus(422);
        $response->assertJson([
            'data' => [
                "contenido" => [],
            ]
        ]);
    }

    public function testNuevoCreaExitosamenteUnNuevoComentario()
    {
        $data = [
            'contenido' => 'Que buen framework',
            'id_framework' => 2,
            'id_user' => 1
        ];

        $response = $this->json('post','api/comentarios', $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => $data]);
    }

    public function testNuevoMuestraErroresDeValidacionConDataErronea()
    {
        $data = [
            'contenido' => 'Que buen framework',
            'id_framework' => 7567,
            'id_user' => 3123
        ];

        $response = $this->json('post', 'api/comentarios', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'data' => [
                "id_framework" => [],
                "id_user" => []
            ]
        ]);
    }


    public function testEliminarLoHaceCorrectamente()
    {
        $id = 2;
        $response = $this->json('delete', 'api/comentarios/' . $id);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }

    public function testEliminarFallaAlIntentarEliminarUnComentarioInexistente()
    {
        $id = 15000;
        $response = $this->json('delete', 'api/comentarios/' . $id);
        $response->assertStatus(404);
    }
}
