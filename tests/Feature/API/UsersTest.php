<?php

namespace Tests\Feature\API;

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function testIndexTraeTodosLosUsuarios()
    {
        $response = $this->json('get','/api/users');

        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
    }

    public function testVerTraeElUsuarioCorrecto()
    {
        $id = 1;
        $response = $this->json('get', 'api/users/' . $id);

        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => ['id' => $id]]);
    }

    public function testVerFallaAlBuscarUnUsuarioInexistente()
    {
        $id = 1000;
        $response = $this->json('get', 'api/users/' . $id);

        $response->assertStatus(404);
    }

    public function testEditarLoHaceCorrectamente()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $data = [
            'name' => 'Nombre test',
            'email' => 'test@test.com'
        ];

        $response = $this->json('put','api/users/', $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => $data]);
    }

    public function testEditarMuestraErroresDeValidacionConDataErronea()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $data = [
            'name' => ''
        ];

        $response = $this->json('put','api/users/', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'success' => false,
            'message' => 'Error de validación',
            'data' => [
                "name" => [],
                "email" => []
            ]
        ]);
    }

    public function testEditarPassLoHaceCorrectamente()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $data = [
            'password' => '123',
            'password_confirmation' => '123'
        ];

        $response = $this->json('put','api/users/pass', $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }

    public function testEditarPassMuestraErroresDeValidacionConDataErronea()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $data = [
            'password' => '1234',
            'password_confirmation' => '123'
        ];

        $response = $this->json('put','api/users/pass', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'success' => false,
            'message' => 'Error de validación',
            'data' => [
                "password" => []
            ]
        ]);
    }

}
