<?php

namespace Tests\Feature\API;

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function testSignUpCreaCorrectamenteElUser()
    {
        $data = [
            'name' => 'prueba',
            'email' => 'prueba@gmail.com',
            'password' => '1234',
            'password_confirmation' => '1234'
        ];
        $response = $this->json('post','/api/auth/signup', $data);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
    }

    public function testSignUpMuestraErroresDeValidacionConDataErronea()
    {
        $data = [
            'name' => 'prueba',
            'email' => 'prueba',
            'password' => '1234',
            'password_confirmation' => '12345'
        ];
        $response = $this->json('post','/api/auth/signup', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'success' => false,
            'message' => 'Error de validación',
            'data' => [
                "email" => [],
                "password" => []
            ]
        ]);
    }

    public function testLoginLogueaCorrectamente()
    {
        Artisan::call('passport:install');
        $data = [
            'email' => 'gon@gmail.com',
            'password' => '1234'
        ];
        $response = $this->json('post','/api/auth/login', $data);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true
        ]);
    }

    public function testLoginMuestraErrorDeAutentificacionConDataIncorrecta()
    {
        $data = [
            'email' => 'gon@gmail.com',
            'password' => '12345'
        ];
        $response = $this->json('post','/api/auth/login', $data);
        $response->assertStatus(401);
        $response->assertJson([
            'success' => false,
            'message' => 'Los datos ingresados no coinciden.'
        ]);
    }

    public function testLoginMuestraErroresDeValidacionConDataErronea()
    {
        $data = [
            'password' => '12345'
        ];
        $response = $this->json('post','/api/auth/login', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'success' => false,
            'message' => 'Error de validación',
            'data' => [
                "email" => []
            ]
        ]);
    }

    /*
     * Tira error 405 (Método no permitido) en vez de 200

    public function testLogoutDeslogueaCorrectamente()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $response = $this->json('post','/api/auth/logout');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'message' => 'Te has deslogueado correctamente'
        ]);
    }
    */
    public function testLogoutNoDeslogueaPorqueSeIngresoUnTokenIncorrecto()
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer TOKENINCORRECTO'
        ];

        $response = $this->json('post','/api/auth/logout', ['none' => 'none'], $headers);
        $response->assertStatus(405);
    }

    /*
    * Tira error 405 (Método no permitido) en vez de 200
    public function testUserDataTraeLaDataCorrectamente()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $response = $this->json('post','/api/auth/user');
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'data' => []
        ]);
    }
    */

    public function testUserDataDevuelveErrorPorqueSeIngresoUnTokenIncorrecto()
    {
        $headers = [
            'Authorization' => 'Bearer TOKENINCORRECTO'
        ];

        $response = $this->json('post','/api/auth/user', ['none' => 'none'], $headers);
        $response->assertStatus(405);
    }
}
