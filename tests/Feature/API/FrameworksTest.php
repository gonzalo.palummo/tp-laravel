<?php

namespace Tests\Feature\API;

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FrameworksTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function setUp() : void
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function testIndexTraeTodosLosFrameworks()
    {
        $response = $this->json('get','/api/frameworks');

        $response->assertStatus(200);
        $response->assertJsonCount(8, 'data');
    }

    public function testVerTraeElFrameworkCorrecto()
    {
        $id = 2;
        $response = $this->json('get', 'api/frameworks/' . $id);

        $response->assertStatus(200);
        $response->assertJson(['data' => ['id' => $id]]);
    }

    public function testVerFallaAlBuscarUnFrameworkInexistente()
    {
        $id = 15000;
        $response = $this->json('get', 'api/frameworks/' . $id);

        $response->assertStatus(404);
    }


    public function testEditarLoHaceCorrectamente()
    {
        $id = 2;
        $data = [
            'nombre' => 'Nombre editado',
            'year' => 1999,
            'id_estrellas' => 4,
            'creador' => 'Javier Palummo'
        ];

        $response = $this->json('put','api/frameworks/'.$id, $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => $data]);
    }

    public function testEditarMuestraErroresDeValidacionConDataErronea()
    {
        $id = 2;
        $data = [
            'nombre' => 'Producto test',
            //'year' => 1980,
            'id_estrellas' => 4,
            //'creador' => 'Gonzalo Palummo'
            'foto' => 'asd'
        ];

        $response = $this->json('put','api/frameworks/'.$id, $data);
        $response->assertStatus(422);
        $response->assertJson([
            'data' => [
                "year" => [],
                "creador" => [],
                "foto" => []
            ]
        ]);
    }

    public function testNuevoCreaExitosamenteUnNuevoFramework()
    {
        $data = [
            'nombre' => 'Producto test',
            'year' => 1980,
            'creador' => 'Gonzalo Palummo',
            'id_estrellas' => 4
        ];

        $response = $this->json('post','api/frameworks', $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true, 'data' => $data]);
    }

    public function testNuevoMuestraErroresDeValidacionConDataErronea()
    {
        $data = [
            'nombre' => 'Producto test',
            //'year' => 1980,
            'id_estrellas' => 4,
            'creador' => 'Gonzalo Palummo',
            'foto' => 'asd'
        ];

        $response = $this->json('post', 'api/frameworks', $data);
        $response->assertStatus(422);
        $response->assertJson([
            'data' => [
                "foto" => [],
                "year" => []
            ]
        ]);
    }


    public function testEliminarLoHaceCorrectamente()
    {
        $id = 2;
        $response = $this->json('delete', 'api/frameworks/' . $id);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }

    public function testEliminarFallaAlIntentarEliminarUnFrameworkInexistente()
    {
        $id = 15000;
        $response = $this->json('delete', 'api/frameworks/' . $id);
        $response->assertStatus(404);
    }
}
