<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;

class ComentariosController extends Controller
{
    public function crear(Request $request)
    {
        $request->validate(Comentario::$rules, Comentario::$errorMessages);
        $data = $request->all();
        Comentario::create($data);
        return redirect(url('/frameworks/'.$data['id_framework']))->with('success', 'Has creado el comentario.');
    }

    public function eliminar($id)
    {
        $comentario = Comentario::findOrFail($id);

        $comentario->delete();

        return redirect(url('/admin/comentarios'))->with('success', 'Has eliminado el comentario.');
    }
}
