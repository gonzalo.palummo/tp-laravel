<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function formLogin()
    {
        return view('login');
    }

    public function doLogin(Request $request)
    {
        $request->validate(User::$loginRules, User::$errorMessages);


        $credenciales = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        if(!Auth::attempt($credenciales)) {
            return redirect(url('login'))
                ->withInput($request->all())
                ->with('message', 'Las credenciales introducidas no coinciden con nuestros registros, intentá de nuevo.');
        }

        return redirect(url('/admin'));
    }

    public function doLogout()
    {
        Auth::logout();
        return redirect(url('/'))->with('message', 'Has cerrado sesión.');
    }
}
