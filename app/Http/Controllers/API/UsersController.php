<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::with(['comentarios'])->orderBy('created_at', 'desc')->get();
        return response()->json([
            'success' => true,
            'data' => $users
        ]);
    }

    public function ver($id)
    {
        $user = User::with(['comentarios'])->findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }

    public function edit(Request $request)
    {

        /** @var array Reglas de validación */
        $rules = [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'email', 'max:100']
        ];

        /** @var array Mensajes de error de las $rules */
        $errorMessages = [
        'name.required' => 'El campo nombre está vacío.',
        'name.string' => 'El campo nombre no es una palabra.',
        'name.max' => 'El campo nombre debe tener hasta :max caracteres.',
        'email.required' => 'El campo email está vacío.',
        'email.email' => 'No se ha ingresado un email.',
        'email.max' => 'El campo email debe tener hasta :max caracteres.'
        ];

        $validation = Validator::make($request->all(), $rules, $errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $data = $request->input();
        $user = User::findOrFail($request->user()->id);
        $user->update($data);

        return response()->json([
            'success' => true,
            'message' => 'Has editado tu usuario correctamente.',
            'data' => $data
        ]);
    }

    public function editPass(Request $request){

        /** @var array Reglas de validación */
        $rules = [
            'password' => ['required', 'string', 'confirmed']
        ];

        /** @var array Mensajes de error de las $rules */
        $errorMessages = [
            'password.required' => 'El campo contraseña está vacio.',
            'password.string' => 'El campo contraseña no es una palabra.',
            'password.confirmed' => 'Las contraseñas no son iguales.',
        ];

        $validation = Validator::make($request->all(), $rules, $errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $user = User::findOrFail($request->user()->id);
        $data = [
            'password' => Hash::make($request->password)
        ];
        $user->update($data);

        return response()->json([
            'success' => true,
            'message' => 'Has editado tu contraseña correctamente.'
        ]);
    }

}
