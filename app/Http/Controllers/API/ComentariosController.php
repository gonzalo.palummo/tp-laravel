<?php

namespace App\Http\Controllers\API;

use App\Models\Comentario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ComentariosController extends Controller
{
    public function index()
    {
        $comentarios = Comentario::with(['framework', 'user'])->orderBy('created_at', 'desc')->get();
        return response()->json([
            'success' => true,
            'data' => $comentarios
        ]);
    }

    public function ver($id)
    {
        $comentario = Comentario::findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $comentario
        ]);
    }

    public function crear(Request $request)
    {
        $validation = Validator::make($request->all(), Comentario::$rules, Comentario::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $data = $request->all();

        Comentario::create($data);

        return response()->json([
            'success' => true,
            'message' => 'Has creado el comentario correctamente.',
            'data' => $data
        ]);
    }


    public function edit($id, Request $request)
    {
        $validation = Validator::make($request->all(), Comentario::$rules, Comentario::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $data = $request->input();
        $comentario = Comentario::findOrFail($id);

        $comentario->update($data);

        return response()->json([
            'success' => true,
            'message' => 'Has editado el comentario correctamente.',
            'data' => $data
        ]);
    }


    public function eliminar($id, Request $request)
    {
        $comentario = Comentario::findOrFail($id);

        $comentario->delete();
        return response()->json([
            'success' => true,
            'message' => 'Has eliminado el comentario con ID '.$id.' correctamente.'
        ]);
    }

}
