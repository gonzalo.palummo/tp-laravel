<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $data = $request->all();
        $validation = Validator::make($data, RegisterController::$rules, RegisterController::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'level' => 'user'
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Usuario creado correctamente'
        ], 201);
    }

    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), User::$loginRules, User::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $credenciales = request(['email', 'password']);
        if (!Auth::attempt($credenciales)) {
            return response()->json([
                'success' => false,
                'message' => 'Los datos ingresados no coinciden.'
            ], 401);
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        return response()->json([
            'success' => true,
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                ->toDateTimeString(),
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'success' => true,
            'message' => 'Te has deslogueado correctamente'
        ]);
    }

    public function user(Request $request)
    {
        $data = $request->user();
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

}
