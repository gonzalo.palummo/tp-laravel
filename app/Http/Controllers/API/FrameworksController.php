<?php

namespace App\Http\Controllers\API;

use App\Models\Framework;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FrameworksController extends Controller
{
    public function index()
    {
        $frameworks = Framework::with(['estrellas', 'comentarios'])->orderBy('id_estrellas', 'desc')->get();
        return response()->json([
            'success' => true,
            'data' => $frameworks
        ]);
    }

    public function ver($id)
    {
        $framework = Framework::with(['estrellas', 'comentarios'])->findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $framework
        ]);
    }

    public function edit($id, Request $request)
    {
        $validation = Validator::make($request->all(), Framework::$rules, Framework::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }

        $data = $request->input();

        if($request->hasFile('foto')) {
            $archivo = $request->foto;
            $nombreFoto = time() . "." . $archivo->extension();
            $archivo->move(public_path('/images'), $nombreFoto);
            $data['foto'] = '/images/'.$nombreFoto;
        }

        $framework = Framework::findOrFail($id);

        $framework->update($data);

        return response()->json([
            'success' => true,
            'message' => 'Has editado el framework correctamente.',
            'data' => $data
        ]);
    }
    public function crear(Request $request)
    {
        $validation = Validator::make($request->all(), Framework::$rules, Framework::$errorMessages);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json([
                'success' => false,
                'message' => 'Error de validación',
                'data' => $errors
            ], 422);
        }


        $data = $request->all();

        if($request->hasFile('foto')) {
            $archivo = $request->foto;
            $nombreFoto = time() . "." . $archivo->extension();
            $archivo->move(public_path('/images'), $nombreFoto);
            $data['foto'] = '/images/'.$nombreFoto;
        } else {
            $data['foto'] = '';
        }

        Framework::create($data);

        return response()->json([
            'success' => true,
            'message' => 'Has creado el framework correctamente.',
            'data' => $data
        ]);
    }

    public function eliminar($id)
    {
        $framework = Framework::findOrFail($id);
        $framework->delete();
        DB::table('comentarios')->where('id_framework', '=', $id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Has eliminado el framework con ID '.$id.' correctamente.'
        ]);
    }

}
