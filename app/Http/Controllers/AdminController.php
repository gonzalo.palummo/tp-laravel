<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use App\Models\Framework;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $frameworks = Framework::with(['estrellas'])->orderBy('id_estrellas', 'asc')->get();
        return view('admin.index', [
            'frameworks' => $frameworks
        ]);
    }

    public function comentarios()
    {
        $comentarios = Comentario::with(['user', 'framework'])->orderBy('created_at', 'desc')->get();
        return view('admin.comentarios', [
            'comentarios' => $comentarios
        ]);
    }
}
