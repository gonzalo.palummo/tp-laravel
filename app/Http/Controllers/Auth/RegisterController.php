<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    /**
     * Crear una nueva instancia del controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /** @var array Reglas de validación. */
    public static $rules = [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'email', 'max:100', 'unique:users'],
        'password' => ['required', 'confirmed'],
    ];

    /** @var array Mensajes de error de las $rules */
    public static $errorMessages = [
        'name.required' => 'El campo nombre está vacío.',
        'name.string' => 'El campo nombre no es una palabra.',
        'name.max' => 'El campo nombre debe tener hasta :max caracteres.',
        'email.required' => 'El campo email está vacío.',
        'email.email' => 'No se ha ingresado un email.',
        'email.max' => 'El campo email debe tener hasta :max caracteres.',
        'email.unique' => 'El email ingresado ya existe.',
        'password.required' => 'El campo contraseña está vacio.',
        'password.confirmed' => 'Las contraseñas no son iguales.',
    ];

    /**
     * Crear el usuario previa validación.
     *
     * @param  Request  $request
     * @return \App\User
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $request->validate($this::$rules, $this::$errorMessages);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'level' => 'user'
        ]);
        return redirect(url('/login'))->with('success', 'Perfecto! Has creado el usuario.');
    }

    public function formRegister()
    {
        return view('register');
    }
}
