<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /** @var array Reglas de validación */
    public static $rules = [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'email', 'max:100']
    ];

    /** @var array Mensajes de error de las $rules */
    public static $errorMessages = [
        'name.required' => 'El campo nombre está vacío.',
        'name.string' => 'El campo nombre no es una palabra.',
        'name.max' => 'El campo nombre debe tener hasta :max caracteres.',
        'email.required' => 'El campo email está vacío.',
        'email.email' => 'No se ha ingresado un email.',
        'email.max' => 'El campo email debe tener hasta :max caracteres.'
    ];

    /** @var array Reglas de validación de password */
    public static $rulesPass = [
        'password' => ['required', 'string', 'confirmed']
    ];

    /** @var array Mensajes de error de las $rulesPass */
    public static $errorMessagesPass = [
        'password.required' => 'El campo contraseña está vacio.',
        'password.string' => 'El campo contraseña no es una palabra.',
        'password.confirmed' => 'Las contraseñas no son iguales.',
    ];

    public function edit(Request $request)
    {
        $validation = Validator::make($request->all(), $this::$rules, $this::$errorMessages);
        if($validation->fails()){
            return redirect(url('/'))->with('errors', $validation->errors());
        }
        $data = $request->input();
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());

        $user->update($data);
        return redirect(url('/'))->with('success', 'Has editado tus datos correctamente.');
    }

    public function editPass(Request $request)
    {
        $validation = Validator::make($request->all(), $this::$rulesPass, $this::$errorMessagesPass);
        if($validation->fails()){
            return redirect(url('/'))->with('errors', $validation->errors());
        }

        $data = [
            'password' => Hash::make($request->password)
        ];
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());

        $user->update($data);
        return redirect(url('/'))->with('success', 'Has editado tu contraseña correctamente.');
    }

}
