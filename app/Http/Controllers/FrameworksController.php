<?php

namespace App\Http\Controllers;

use App\Models\Framework;
use App\Models\Estrellas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FrameworksController extends Controller
{/*
    public function index()
    {
        $frameworks = Framework::with(['estrellas'])->orderBy('id_estrellas', 'desc')->get();
        return view('frameworks.index', [
            'frameworks' => $frameworks
        ]);
    }
*/
    public function ver($id)
    {
        $framework = Framework::findOrFail($id);
        return view('frameworks.ver', [
            'framework' => $framework
        ]);
    }

    public function formNuevo()
    {
        $estrellas = Estrellas::all();
        return view('frameworks.new', compact('estrellas'));
    }

    public function formEdit($id)
    {
        $framework = Framework::findOrFail($id);
        $estrellas = Estrellas::all();
        return view('frameworks.edit', compact(['framework', 'estrellas']));
    }

    public function edit($id, Request $request)
    {
        $request->validate(Framework::$rules, Framework::$errorMessages);
        $data = $request->input();

        if($request->hasFile('foto')) {
        $archivo = $request->foto;
        $nombreFoto = time() . "." . $archivo->extension();
        $archivo->move(public_path('/images'), $nombreFoto);
        $data['foto'] = '/images/'.$nombreFoto;
    }

        $framework = Framework::findOrFail($id);

        $framework->update($data);
        return redirect(url('/admin'))->with('success', 'Has editado el framework correctamente.');
    }

    public function crear(Request $request)
    {
        $request->validate(Framework::$rules, Framework::$errorMessages);

        $data = $request->all();

        if($request->hasFile('foto')) {
            $archivo = $request->foto;
            $nombreFoto = time() . "." . $archivo->extension();
            $archivo->move(public_path('/images'), $nombreFoto);
            $data['foto'] = '/images/'.$nombreFoto;
        } else {
            $data['foto'] = '';
        }

        Framework::create($data);
        return redirect(url('/admin'));
    }

    public function eliminar($id)
    {
        $framework = Framework::findOrFail($id);

        $framework->delete();
        DB::table('comentarios')->where('id_framework', '=', $id)->delete();

        return redirect(url('/admin'))->with('success', 'Has eliminado el framework.');
    }
}
