<?php

namespace App\Http\Controllers;

use App\Models\Framework;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $frameworks = Framework::with(['estrellas'])->orderBy('id_estrellas', 'asc')->get();
        return view('index', [
            'frameworks' => $frameworks
        ]);
    }
}
