<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();

        if($data['id_user'] == $request->user()->id){
            return $next($request);
        }

        return response()->json([
            'success' => false,
            'message' => 'Error de autentificación de token.'
        ]);
    }
}
