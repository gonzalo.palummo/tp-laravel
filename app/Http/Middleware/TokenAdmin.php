<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class TokenAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->level == 'admin'){
            return $next($request);
        }

        return response()->json([
            'success' => false,
            'message' => 'Necesitas nivel de administrador.'
        ]);
    }
}
