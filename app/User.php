<?php

namespace App;

use App\Models\Comentario;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'name', 'level'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at',
    ];

    /** @var array Reglas de validación. */
    public static $loginRules = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    /** @var array Mensajes de error de las $loginRules */
    public static $errorMessages = [
        'email.required' => 'El campo email está vacío.',
        'email.email' => 'No se ha ingresado un email.',
        'password.required' => 'El campo contraseña está vacio.',
    ];

    /**
     * Define la relación con comentarios.
     * Crea la propiedad "comentarios" (el nombre del método) que sería
     * en este caso, un array de Comentario.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'id_user');
    }
}
