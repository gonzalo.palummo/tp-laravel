<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Comentario extends Model
{
    use SoftDeletes;

    /** @var string Nombre de tabla. */
    protected $table = "comentarios";

    /** @var array Lista de campos que se pueden cargar masivamente. */
    protected $fillable = ['id_user', 'id_framework', 'contenido'];

    /** @var array Reglas de validación. */
    public static $rules = [
        'id_user' => 'integer|exists:users,id',
        'id_framework' => 'integer|exists:frameworks,id',
        'contenido' => 'required',
    ];

    /** @var array Mensajes de error de las $rules */
    public static $errorMessages = [
        'id_user.integer' => 'El usuario debe ser un número entero.',
        'id_user.exists' => 'El usuario debe existir.',
        'id_framework.integer' => 'El framework debe ser un número entero.',
        'id_framework.exists' => 'El framework debe existir.',
        'contenido.required' => 'El comentario es requerido.'
    ];

    /** Se define la relación con User */
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    /** Se define la relación con Framework */
    public function framework()
    {
        return $this->belongsTo(Framework::class, 'id_framework');
    }
}
