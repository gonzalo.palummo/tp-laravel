<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estrellas extends Model
{
    /** @var string Nombre de tabla. */
    protected $table = "estrellas";

    /**
     * Define la relación con frameworks.
     * Crea la propiedad "frameworks" (el nombre del método) que sería
     * en este caso, un array de Framework.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function frameworks()
    {
        return $this->hasMany(Framework::class, 'id_estrellas');
    }
}
