<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Framework extends Model
{
    use SoftDeletes;

    /** @var string Nombre de tabla. */
    protected $table = "frameworks";

    /** @var array Lista de campos que se pueden cargar masivamente. */
    protected $fillable = ['id_estrellas', 'foto', 'nombre', 'year', 'creador'];

    /** @var array Reglas de validación. */
    public static $rules = [
        'id_estrellas' => 'required|integer|exists:estrellas,id',
        'foto' => 'file',
        'nombre' => 'required|min:3',
        'year' => 'required|numeric',
        'creador' => 'required|string'
    ];

    /** @var array Mensajes de error de las $rules */
    public static $errorMessages = [
        'id_estrellas.required' => 'El framework debe tener un mínimo de estrellas.',
        'id_estrellas.integer' => 'Las estrellas deben ser un ID.',
        'id_estrellas.exists' => 'El ID de estrellas debe existir.',
        'foto.file' => 'No se ha cargado un archivo',
        'nombre.required' => 'El framework debe tener un nombre.',
        'nombre.min' => 'El nombre del framework debe tener al menos :min caracteres.',
        'year.required' => 'El framework debe tener un año de lanzamiento.',
        'year.numeric' => 'El año de lanzamiento debe ser un número.',
        'creador.required' => 'El framework debe tener un creador.',
        'creador.string' => 'El creador debe ser una palabra.'
    ];

    /** Se define la relación con Estrellas */
    public function estrellas()
    {
        return $this->belongsTo(Estrellas::class, 'id_estrellas');
    }

    /**
     * Define la relación con comentarios.
     * Crea la propiedad "comentarios" (el nombre del método) que sería
     * en este caso, un array de Comentario.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'id_framework');
    }
}
