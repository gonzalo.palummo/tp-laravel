<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdEstrellasToFrameworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('frameworks', function (Blueprint $table) {
            $table->unsignedInteger('id_estrellas')->after('id')->default(0);
            $table->foreign('id_estrellas')->references('id')->on('estrellas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('frameworks', function (Blueprint $table) {
            $table->dropForeign(['id_estrellas']);
            $table->dropColumn('id');
        });
    }
}
