<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EstrellasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estrellas')->insert([
            'id' => 1,
            'cantidad' => 'Más de 130.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estrellas')->insert([
            'id' => 2,
            'cantidad' => 'Más de 120.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estrellas')->insert([
            'id' => 3,
            'cantidad' => 'Más de 100.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estrellas')->insert([
            'id' => 4,
            'cantidad' => 'Más de 40.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estrellas')->insert([
            'id' => 5,
            'cantidad' => 'Más de 20.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('estrellas')->insert([
            'id' => 6,
            'cantidad' => 'Más de 10.000 estrellas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
