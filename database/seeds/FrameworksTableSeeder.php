<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class FrameworksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('frameworks')->insert([
            'id' => 1,
            'id_estrellas' => 4,
            'foto' => '/images/angular.png',
            'nombre' => 'Angular',
            'year' => '2016',
            'creador' => 'Google',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 2,
            'id_estrellas' => 4,
            'foto' => '/images/angularjs.jpg',
            'nombre' => 'Angular JS',
            'year' => '2010',
            'creador' => 'Google',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 3,
            'id_estrellas' => 2,
            'foto' => '/images/react.png',
            'nombre' => 'React',
            'year' => '2013',
            'creador' => 'Facebook',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 4,
            'id_estrellas' => 5,
            'foto' => '/images/ember.png',
            'nombre' => 'Ember JS',
            'year' => '2011',
            'creador' => 'Yehuda Katz',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 5,
            'id_estrellas' => 4,
            'foto' => '/images/meteor.png',
            'nombre' => 'Meteor',
            'year' => '2012',
            'creador' => 'Meteor Development Group',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 6,
            'id_estrellas' => 1,
            'foto' => '/images/vue.jpeg',
            'nombre' => 'Vue.js',
            'year' => '2014',
            'creador' => 'Evan You',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 7,
            'id_estrellas' => 4,
            'foto' => '/images/jquery.gif',
            'nombre' => 'jQuery',
            'year' => '2006',
            'creador' => 'John Resig',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('frameworks')->insert([
            'id' => 8,
            'id_estrellas' => 5,
            'foto' => '/images/backbone.png',
            'nombre' => 'Backbone',
            'year' => '2010',
            'creador' => 'Jeremy Ashkenas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
