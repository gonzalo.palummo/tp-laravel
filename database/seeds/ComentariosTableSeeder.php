<?php

use Illuminate\Database\Seeder;

class ComentariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comentarios')->insert([
            'id' => 1,
            'contenido' => 'El mejor framework desde mi punto de vista',
            'id_framework' => '1',
            'id_user' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('comentarios')->insert([
            'id' => 2,
            'contenido' => 'No conozco gente que trabaje con este',
            'id_framework' => '2',
            'id_user' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
