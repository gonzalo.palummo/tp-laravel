@extends('layouts.main')

@section('title', 'Iniciar Sesión')

@section('main')
    <div class="container my-5 col-5">
        <h1 class="text-center">Iniciar Sesión</h1>
        <p class="text-center">Completá los datos.</p>

        @if(Session::has('message'))
            <div class="alert alert-danger">{{ Session::get('message') }}</div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success my-3">{{ Session::get('success') }}</div>
        @endif
        <form action="{{ url('login') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="email">Correo:</label>
                <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}">
                @if($errors->has('email'))
                    <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password" class="form-control">
                @if($errors->has('password'))
                    <div class="alert alert-danger">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <button class="btn btn-cool btn-block">Ingresar</button>
            <a class="btn btn-secondary btn-block" href="{{url('/register')}}">Ir al registro</a>
        </form>
        <div class="text-center">
            <a class="btn btn-primary m-4" href="{{url('/')}}">Ir al home</a>
        </div>
    </div>
@endsection