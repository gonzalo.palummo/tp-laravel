@extends('layouts.main')

@section('title', 'Comentarios Admin')
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-black">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('admin') }}">Frameworks</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" href="{{ url('admin/comentarios') }}">Comentarios</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                </li>
            </ul>
        </div>
    </nav>
@endsection
@section('main')
    <div class="portada py-5">
        <h1 class="text-center mb-4">Administrar comentarios</h1>
        <div class="container">
            @if(Session::has('success'))
                <div class="alert alert-success my-3">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger my-3">{{ Session::get('error') }}</div>
            @endif
        </div>
        @if(count($comentarios))
            <table class="table table-dark text-center">
                <thead>
                    <tr>
                        <th scope="col">Contenido</th>
                        <th scope="col">Usuario</th>
                        <th scope="col">Framework</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($comentarios as $comentario)
                    <tr>
                        <td>{{$comentario->contenido}}</td>
                        <td>{{$comentario->user->name}}</td>
                        <td>{{$comentario->framework->nombre}}</td>
                        <td>
                            <form action="{{url('comentarios/delete/'.$comentario->id)}}" onsubmit="if(!confirm('¿Seguro que desea eliminar el comentario de {{$comentario->user->name}}?')){return false;}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="text-danger btn btn-link"><i class="material-icons">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p class="text-center">No hay comentarios.</p>
        @endif
    </div>
@endsection