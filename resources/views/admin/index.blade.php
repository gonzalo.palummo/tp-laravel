@extends('layouts.main')

@section('title', 'Portada Admin')
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-black">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" href="{{ url('admin') }}">Frameworks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('admin/comentarios') }}">Comentarios</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                </li>
            </ul>
        </div>
    </nav>
@endsection
@section('main')
    <div class="portada py-5">
        <h1 class="text-center">Administrar frameworks</h1>
        <div class="container">
            @if(Session::has('success'))
                <div class="alert alert-success my-3">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger my-3">{{ Session::get('error') }}</div>
            @endif
            <a class="btn btn-light btn-block col-6 my-3 mx-auto" href="{{url('/frameworks/new')}}">Nuevo framework</a>
        </div>
        @if(count($frameworks))
            <table class="table table-dark text-center">
                <thead>
                    <tr>
                        <th scope="col">Foto</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Año de lanzamiento</th>
                        <th scope="col">N° de estrellas en GitHub</th>
                        <th scope="col">Ver</th>
                        <th scope="col">Editar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($frameworks as $framework)
                    <tr>
                        <td>
                        @if($framework->foto != '')
                            <a href="{{ url('frameworks/' . $framework->id) }}"><img src="{{url($framework->foto)}}" alt="{{$framework->nombre}}" class="foto"></a>
                        @else
                            Sin foto
                        @endif
                        </td>
                        <td>{{$framework->nombre}}</td>
                        <td>{{$framework->year}}</td>
                        <td>{{$framework->estrellas->cantidad}}</td>
                        <td><a href="{{ url('frameworks/' . $framework->id) }}" class="btn btn-link text-white"><i class="material-icons">remove_red_eye</i></a></td>
                        <td><a href="{{ url('frameworks/edit/' . $framework->id) }}" class="btn btn-link text-primary"><i class="material-icons">border_color</i></a></td>
                        <td>
                                <form action="{{url('frameworks/delete/'.$framework->id)}}" onsubmit="if(!confirm('¿Seguro que desea el framework {{$framework->nombre}}?')){return false;}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="text-danger btn btn-link"><i class="material-icons delete">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p class="text-center">No hay frameworks agregados.</p>
        @endif
    </div>
@endsection