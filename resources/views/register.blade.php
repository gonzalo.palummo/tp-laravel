@extends('layouts.main')

@section('title', 'Registrarse')

@section('main')
    <div class="container my-5 col-5">
        <h1 class="text-center">Registrarse</h1>
        <p class="text-center">Completá los datos.</p>

        @if(Session::has('message'))
            <div class="alert alert-danger">{{ Session::get('message') }}</div>
        @endif

        <form action="{{ url('register') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}">
                @if($errors->has('name'))
                    <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Correo:</label>
                <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}">
                @if($errors->has('email'))
                    <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Contraseña:</label>
                <input type="password" id="password" name="password" class="form-control">
                @if($errors->has('password'))
                    <div class="alert alert-danger">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="password_confirmation">Repetir contraseña:</label>
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
            </div>
            <button class="btn btn-cool btn-block">Registrarse</button>
            <a class="btn btn-secondary btn-block" href="{{url('/login')}}">Ir al login</a>
        </form>
        <div class="text-center">
            <a class="btn btn-primary m-4" href="{{url('/')}}">Ir al home</a>
        </div>
    </div>
@endsection