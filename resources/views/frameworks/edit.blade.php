@extends('layouts.main')
@section('title', 'Editar '.$framework->nombre)
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-black">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" href="{{ url('admin') }}">Frameworks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('admin/comentarios') }}">Comentarios</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                </li>
            </ul>
        </div>
    </nav>
@endsection
@section('main')
   <div class="container my-5 col-6">
       <div class="text-center">
           <h1 class="m-4">Editar framework</h1>
           @if($framework->foto != '')
               <img src="{{url($framework->foto)}}" alt="{{$framework->nombre}}" class="foto">
           @endif
       </div>
        <form action="{{ url('frameworks/'.$framework->id) }}" method="post" enctype="multipart/form-data">
            @method('put')
            @include('frameworks._form')
        </form>
   </div>
   <div class="text-center">
       <a class="btn btn-cool m-4" href="{{url('/admin')}}">Volver</a>
   </div>
@endsection
@section('scripts')
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
@endsection