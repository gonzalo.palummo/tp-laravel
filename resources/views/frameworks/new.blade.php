@extends('layouts.main')
@section('title', 'Nuevo framework')
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-black">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link active" href="{{ url('admin') }}">Frameworks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('admin/comentarios') }}">Comentarios</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                </li>
            </ul>
        </div>
    </nav>
@endsection
@section('main')
    <div class="container my-5 col-6">
        <h1 class="text-center m-4">Nuevo framework</h1>
        <form action="{{ url('frameworks/new') }}" method="post" enctype="multipart/form-data">
        @include('frameworks._form')
        </form>
        <div class="text-center">
            <a class="btn btn-cool m-4" href="{{url('/admin')}}">Volver</a>
        </div>
    </div>
@endsection
@section('scripts')
    $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
@endsection