@extends('layouts.main')

@section('title', $framework->nombre)
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                @if(Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('login') }}">Iniciar Sesión / Registro</a>
                    </li>
                @elseif(Auth::user()->level == 'admin')
                    <li class="nav-item active">
                        <a class="nav-link active" href="{{ url('admin') }}">Frameworks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('admin/comentarios') }}">Comentarios</a>
                    </li>
                @endif
                @if(Auth::check())
                    <li class="nav-item">
                        <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
@endsection
@section('main')
    <div class="py-5 container">
        <h1 class="text-center">{{$framework->nombre}}</h1>
        <dl class="text-center pb-4">
            @if($framework->foto != '')
                <dt class="h4 mt-4 d-none">Foto</dt>
                <dd><img src="{{url($framework->foto)}}" alt="{{$framework->nombre}}"></dd>
            @endif
            <dt class="h4 mt-4">Nombre</dt>
            <dd>{{$framework->nombre}}</dd>
            <dt class="h4 mt-4">Año de lanzamiento</dt>
            <dd>{{$framework->year}}</dd>
            <dt class="h4 mt-4">N° de estrellas en GitHub</dt>
            <dd>{{$framework->estrellas->cantidad}}</dd>
            <dt class="h4 mt-4">Creador</dt>
            <dd>{{$framework->creador}}</dd>
        </dl>
        <hr />
        <h2 class="text-center mt-5">Comentarios</h2>
        @if(Session::has('success'))
            <div class="alert alert-success my-3">{{ Session::get('success') }}</div>
        @endif
        @if(Auth::guest())
            <p class="text-center">Para comentar es necesario estar logueado. Ingresa desde <a href="{{ url('login') }}">acá</a>.</p>
        @else
            <form action="{{url('comentarios/new')}}" method="post" class="text-right w-50 mx-auto">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="contenido" cols="30" rows="5" placeholder="Ingrese el comentario..."></textarea>
                </div>
                @if($errors->has('contenido'))
                    <div class="alert alert-danger text-left">{{ $errors->first('contenido') }}</div>
                @endif
                <input type="hidden" name="id_framework" value="{{$framework->id}}">
                <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
                <button class="btn btn-cool">Enviar</button>
            </form>
        @endif

        @if(count($framework->comentarios))
            <div class="px-4 w-50 mx-auto">
                @foreach($framework->comentarios->sortByDesc('created_at') as $comentario)
                    <div class="row btn-cool py-3 my-3 rounded">
                        <h3 class="col-sm-8">{{$comentario->user->name}}</h3>
                        <span class="col-sm-4 text-right">{{$comentario->user->created_at->format('d/m/Y H:i')}}</span>
                        <p class="col-sm-12">{{$comentario->contenido}}</p>
                    </div>
                @endforeach
            </div>
        @else
            <p class="text-center">No hay comentarios.</p>
        @endif
    </div>

    @if(Auth::guest())
        <div class="text-center">
            <a class="btn btn-cool m-4" href="{{url('/')}}">Volver</a>
        </div>
    @else
        <div class="text-center">
            <a class="btn btn-cool m-4" href="{{url('admin')}}">Volver</a>
        </div>
    @endif
@endsection