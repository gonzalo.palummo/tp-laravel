@csrf
<div class="form-group">
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" class="form-control" value="@isset($framework){{old('nombre', $framework->nombre)}}@else{{old('nombre')}}@endisset">
    @if($errors->has('nombre'))
        <div class="alert alert-danger">{{ $errors->first('nombre') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="year">Año de lanzamiento:</label>
    <input type="number" id="year" name="year" class="form-control" value="@isset($framework){{old('year', $framework->year)}}@else{{old('year')}}@endisset">
    @if($errors->has('year'))
        <div class="alert alert-danger">{{ $errors->first('year') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="estrellas" class="d-block">N° de estrellas en GitHub:</label>
    <select name="id_estrellas" id="estrellas" class="form-control">
        @foreach($estrellas as $estrella)
            <option value="{{$estrella->id}}"
                @isset($framework)
                    @if($estrella->id == old('id_estrellas', $framework->id_estrellas))
                        selected
                    @endif
                @else
                    @if($estrella->id == old('id_estrellas'))
                        selected
                    @endif
                @endisset
            >{{$estrella->cantidad}}</option>
        @endforeach
    </select>
    @if($errors->has('id_estrellas'))
        <div class="alert alert-danger">{{ $errors->first('id_estrellas') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="creador">Creador:</label>
    <input type="text" id="creador" name="creador" class="form-control" value="@isset($framework){{old('creador', $framework->creador)}}@else{{old('creador')}}@endisset">
    @if($errors->has('creador'))
        <div class="alert alert-danger">{{ $errors->first('creador') }}</div>
    @endif
</div>
<div class="custom-file">
    <input type="file" id="foto" name="foto" class="custom-file-input">
    <label for="foto" class="custom-file-label">Foto:</label>
    @if($errors->has('foto'))
        <div class="alert alert-danger">{{ $errors->first('foto') }}</div>
    @endif
</div>
<button class="btn btn-success btn-block my-4">Enviar</button>