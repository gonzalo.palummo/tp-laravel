<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" href="{{ url('css/estilos.css') }}">
    <link rel="icon" href="{{ url('favicon.ico') }}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{url('../resources/js/jquery.min.js')}}"></script>
    <script src="{{url('../resources/js/main.js')}}"></script>
</head>
<body>
@yield('nav')
<main>
    @yield('main')
    @if(Auth::check())
        <div id="modal" class="modal">
            <div class="modal-content">
                <span class="modal-close">&times;</span>
                <h1 class="h2 mb-4 text-center">Editar Mis Datos</h1>
                <form action="{{ url('user') }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="name">Nombre:</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ old('name', Auth::user()->name) }}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Correo:</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{ old('email', Auth::user()->email) }}" required>
                    </div>
                    <button class="btn btn-cool btn-block">Modificar datos</button>
                </form>
                <h1 class="h2 my-4 mt-5 text-center">Editar Contraseña</h1>
                <form action="{{ url('user/pass') }}" method="post">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="password">Nueva Contraseña:</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Repetir nueva contraseña:</label>
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                    </div>
                    <button class="btn btn-cool btn-block">Modificar contraseña</button>
                </form>
            </div>
        </div>
    @endif
</main>
<footer class="main-footer text-center bg-dark py-4">
    <p class="m-0 text-white">Desarrollado por Gonzalo Palummo | 2019</p>
</footer>
<script>
    @yield('scripts')

    $(document).ready(function() {
      if(document.getElementById("modal")){
        let modal = document.getElementById("modal");
        let spanModal = document.querySelector(".modal .modal-close");

        function openModal(ev) {
          ev.preventDefault();
          modal.style.display = "block";
        }

        let links = document.querySelectorAll(".miCuenta");
        links.forEach(function (link) {
          link.addEventListener("click", openModal);
        });

        spanModal.addEventListener('click', function () {
          modal.style.display = "none";
        });

        window.addEventListener('click', function (event) {
          if (event.target == modal) {
            modal.style.display = "none";
          }
        });
      }
    });
</script>
</body>
</html>
