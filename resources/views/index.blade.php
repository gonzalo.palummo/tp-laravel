@extends('layouts.main')

@section('title', 'Portada')
@section('nav')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Frameworks</a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                @if(Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('login') }}">Iniciar Sesión / Registro</a>
                    </li>
                @elseif(Auth::user()->level == 'admin')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('admin') }}">Admin</a>
                    </li>
                @endif
                @if(Auth::check())
                    <li class="nav-item">
                        <a href="{{ url('myAccount') }}" class="nav-link miCuenta">Mi cuenta</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('logout') }}" class="nav-link"> {{ Auth::user()->email }} (Cerrar Sesión)</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
@endsection
@section('main')
    <div class="portada py-5">
        <div class="container">
            @if(Session::has('success'))
                <div class="alert alert-success my-3">{{ Session::get('success') }}</div>
            @endif
            @if(Session::has('error'))
                    <div class="alert alert-danger my-3">{{ Session::get('error') }}</div>
                @endif
            @if($errors->has('name')) <div class="alert alert-danger">{{ $errors->first('name') }}</div> @endif
            @if($errors->has('email')) <div class="alert alert-danger">{{ $errors->first('email') }}</div> @endif
            @if($errors->has('password')) <div class="alert alert-danger">{{ $errors->first('password') }}</div> @endif
        </div>
        <h1 class="text-center">Los frameworks JS para Front-end más usados</h1>
        <div class="pt-4 pb-5">
            <p class="col-6 mx-auto my-5 text-center font-weight-bold">JavaScript sigue creciendo y el número de frameworks y librerías que se utilizan habitualmente comienza a ser muy elevado.</p>
            <p class="col-6 mx-auto my-5 text-center font-weight-bold">Es imposible para los desarrolladores poder conocerlos todos. Pero sí que es importante saber cuales son los más populares.</p>
        </div>
        @if(count($frameworks))
            <table class="table table-dark text-center">
                <thead>
                    <tr>
                        <th scope="col">Foto</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Año de lanzamiento</th>
                        <th scope="col">N° de estrellas en GitHub</th>
                        <th scope="col">Ver</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($frameworks as $framework)
                    <tr>
                        <td>
                            @if($framework->foto != '')
                                <a href="{{ url('frameworks/' . $framework->id) }}"><img src="{{url($framework->foto)}}" alt="{{$framework->nombre}}" class="foto"></a>
                            @else
                                Sin foto
                            @endif
                        </td>
                        <td>{{$framework->nombre}}</td>
                        <td>{{$framework->year}}</td>
                        <td>{{$framework->estrellas->cantidad}}</td>
                        <td><a href="{{ url('frameworks/' . $framework->id) }}" class="text-white"><i class="material-icons">remove_red_eye</i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p class="text-center">No hay frameworks agregados.</p>
        @endif
    </div>
@endsection